#include <stdio.h>

#include "core_comp1.h"

#ifndef TEST
#error "TEST should be defined"
#endif

int main()
{
    printf("Running the test executable\n");

    core_comp1_display();

    return 1;
}