cmake_minimum_required(VERSION 3.10)

# set the project name
project(cmake_test)

# add the subdirectories
add_subdirectory(core)
add_subdirectory(lib)

# define my targets
set(MY_TARGETS myexe mytest)

# create and link against the libs
foreach(target ${MY_TARGETS})
    add_executable(${target})
    target_link_libraries(${target} PRIVATE core ext)
endforeach(target ${MY_TARGETS})

# add target specific sources
target_sources(myexe  PRIVATE main.c)
target_sources(mytest PRIVATE test.c)

# define the TEST macro for mytest
target_compile_definitions(mytest PUBLIC TEST)
