#include <stdio.h>

#if TEST
void core_comp1_display()
{
    printf("Test %s\n", __func__);
}
#else
void core_comp1_display()
{
    printf("Original %s\n", __func__);
}
#endif
